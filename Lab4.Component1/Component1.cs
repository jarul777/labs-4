﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ComponentFramework;
using Lab4.Contract;

namespace Lab4.Component1
{
    public class Component1:AbstractComponent
    {
        IMycie myj;

        public override void InjectInterface(Type type, object impl)
        {
            if (type == typeof(IMycie))
            {
                myj = (IMycie)impl;
            }
        }
        public Component1()
        {
            this.RegisterRequiredInterface(typeof(IMycie));
        }

        public void Czysc()
        {
            Console.WriteLine("Szczotkuj");
        }
        public void Susz()
        {
            Console.WriteLine("Suszarka dziala");
        }

    }
}
