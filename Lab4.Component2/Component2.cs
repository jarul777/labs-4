﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ComponentFramework;
using Lab4.Contract;


namespace Lab4.Component2
{
    public class Component2:AbstractComponent,IMycie
    {
        public Component2()
        {
            this.RegisterProvidedInterface(typeof(IMycie), this);
        }
        void IMycie.Czysc()
        {
           Console.WriteLine("Czysci");
        }

        void IMycie.Susz()
        {
            Console.WriteLine("Suszy");
        }

        public override void InjectInterface(Type type, object impl)
        {
            throw new NotImplementedException();
        }
    }
}
