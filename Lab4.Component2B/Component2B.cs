﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ComponentFramework;
using Lab4.Contract;

namespace Lab4.Component2B
{
    public class Component2B:AbstractComponent,IMycie
    {
        public Component2B()
        {
            this.RegisterProvidedInterface(typeof(IMycie), this);
        }
        public override void InjectInterface(Type type, object impl)
        {
            throw new NotImplementedException();
        }

        void IMycie.Czysc()
        {
            Console.WriteLine("Ręcznie");
        }

        void IMycie.Susz()
        {
            Console.WriteLine("Automatycznie");
        }
    }
}
