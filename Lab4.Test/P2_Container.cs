﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NUnit.Framework;
using Assert = NUnit.Framework.Assert;
using Lab4.Main;

namespace Lab4.Test
{
    [TestFixture]
    [TestClass]
    public class P2_Container
    {
        [Test]
        [TestMethod]
        public void P2__Container_Should_Be_Instantiable()
        {
            // Arrange
            var container = Activator.CreateInstance(LabDescriptor.Container);

            // Assert
            Assert.That(container, Is.Not.Null);
        }

        [Test]
        [TestMethod]
        public void P2__Component1_Should_Be_Instantiable()
        {
            // Arrange
            var component = Activator.CreateInstance(LabDescriptor.Component1);

            // Assert
            Assert.That(component, Is.Not.Null);
        }

        [Test]
        [TestMethod]
        public void P2__Component2_Should_Be_Instantiable()
        {
            // Arrange
            var component = Activator.CreateInstance(LabDescriptor.Component2);

            // Assert
            Assert.That(component, Is.Not.Null);
        }

        [Test]
        [TestMethod]
        public void P2__Dependencies_Should_Be_Resolved_Without_Registering_Any_Component()
        {
            // Arrange
            var container = Activator.CreateInstance(LabDescriptor.Container);

            // Assert
            Assert.That(LabDescriptor.ResolvedDependencied(container));
        }

        [Test]
        [TestMethod]
        public void P2__Dependencies_Should_Not_Be_Resolved_After_Registering_Component1()
        {
            // Arrange
            var container = Activator.CreateInstance(LabDescriptor.Container);
            var component = Activator.CreateInstance(LabDescriptor.Component1);

            // Act
            LabDescriptor.RegisterComponent(container, component);

            // Assert
            Assert.That(!LabDescriptor.ResolvedDependencied(container));
        }

        [Test]
        [TestMethod]
        public void P2__Dependencies_Should_Be_Resolved_After_Registering_Component1_And_Component2()
        {
            // Arrange
            var container = Activator.CreateInstance(LabDescriptor.Container);
            var component1 = Activator.CreateInstance(LabDescriptor.Component1);
            var component2 = Activator.CreateInstance(LabDescriptor.Component2);

            // Act
            LabDescriptor.RegisterComponent(container, component1);
            LabDescriptor.RegisterComponent(container, component2);

            // Assert
            Assert.That(LabDescriptor.ResolvedDependencied(container));
        }
    }
}
