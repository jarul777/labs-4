﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ComponentFramework;
using Lab4.Contract;
using Lab4.Component2B;

namespace Lab4.Main
{
    public class Program
    {
        static void Main(string[] args)
        {
            Component1.Component1 kom1 = new Component1.Component1();
            Component2.Component2 kom2 = new Component2.Component2();
            kom1.Czysc();
            kom1.Susz();
            ((IMycie)kom2).Susz();
            ((IMycie)kom2).Czysc();
            Console.ReadKey();
        }
    }
}
