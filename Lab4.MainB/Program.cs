﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab4.Contract;
using Lab4.Component2B;
using ComponentFramework;

namespace Lab4.MainB
{
    public class Program
    {
        static void Main(string[] args)
        {
            Component1.Component1 kom1 = new Component1.Component1();
            Component2B.Component2B kom2 = new Component2B.Component2B();
            kom1.Czysc();
            kom1.Susz();
            ((IMycie)kom2).Susz();
            ((IMycie)kom2).Czysc();
            Console.ReadKey();
        }
    }
}